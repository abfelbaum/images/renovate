# node images

This project provides the renovate docker image. A scheduled pipeline runs daily to update the images.

Documentation: https://abfelbaum.dev/projects/list/images/dotnet
